###v0.0.12
 * 解决label组件自定义时，一直刷新问题
 * 解决custom infoWindow的offset参数无效问题
 * 解决label自定义模式下，当未初始化完成时更新slot内容导致报错问题
 * 解决label自定义模式下，某些初始化时无内容问题

###V0.0.11
  * 增加自定义窗体组件

###V0.0.10
  * 地图增加defaultCursor属性
  * 修复polygon修改path时，如果处于edit状态，不会触发拖拽图标问题

###V0.0.9
  * 修复label与infoWindow的position无法动态修改问题
  * label增加slot功能

###V0.0.7
  增加车辆跟踪插件

###V0.0.3
  修复编译失败问题


###V0.0.1
  初始化第一个版本，完成：
  * el-map（地图）
  * el-bmap-bezier-curve（贝塞尔曲线）
  * el-bmap-circle（圆）
  * el-bmap-ground-overlay（地面叠加层）
  * el-bmap-info-window（信息窗口）
  * bmap-label（文本标记）
  * bmap-marker（标号）
  * bmap-marker-3d（3D标号）
  * bmap-polygon（多边形）
  * bmap-polyline（折线）
  * bmap-prism（3D棱柱）
